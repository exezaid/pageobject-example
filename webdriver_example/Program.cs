﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using webdriver_example.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Firefox;

namespace webdriver_example
{
    class Program
    {


        static void Main(string[] args)
        {


            IWebDriver driver = new FirefoxDriver();

            HomePage home = new HomePage(driver);
            home.goToPage();
            AboutPage about = home.goToAboutPage();
            ResultPage result = about.search("python" + Keys.Enter);
            result.clickOnFirstArticle();
            


        }
        
}
}